package de.sebse.fuplanner.tools.network;

import androidx.annotation.NonNull;

/**
 * Created by sebastian on 24.10.17.
 */

public class NetworkError {
    private final int code;
    private final int httpStatus;
    private final String message;

    public NetworkError(int code, int httpStatus, String message) {
        this.code = code;
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    private String getMessage() {
        return message;
    }

    @NonNull
    public String toString() {
        return String.valueOf(getCode()) + " - " + getHttpStatus() + " - " + getMessage();
    }
}
