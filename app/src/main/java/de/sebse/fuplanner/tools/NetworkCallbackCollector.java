package de.sebse.fuplanner.tools;

import android.util.Pair;

import java.util.HashSet;
import java.util.Iterator;

import de.sebse.fuplanner.tools.network.NetworkCallback;
import de.sebse.fuplanner.tools.network.NetworkError;
import de.sebse.fuplanner.tools.network.NetworkErrorCallback;

public class NetworkCallbackCollector<T> extends HashSet<Pair<NetworkCallback<T>, NetworkErrorCallback>> {
    public void add(NetworkCallback<T> success, NetworkErrorCallback error) {
        add(new Pair<>(success, error));
    }

    public void responseError(NetworkError error) {
        Iterator<Pair<NetworkCallback<T>, NetworkErrorCallback>> i;
        for (i = this.iterator(); i.hasNext();) {
            Pair<NetworkCallback<T>, NetworkErrorCallback> pair = i.next();
            pair.second.onError(error);
            i.remove();
        }
    }

    public void responseResponse(T success) {
        Iterator<Pair<NetworkCallback<T>, NetworkErrorCallback>> i;
        for (i = this.iterator(); i.hasNext();) {
            Pair<NetworkCallback<T>, NetworkErrorCallback> pair = i.next();
            pair.first.onResponse(success);
            i.remove();
        }
    }
}
