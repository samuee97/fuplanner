package de.sebse.fuplanner.tools;

public interface RequestPermissionsResultListener {
    void callback(int requestCode, String[] permissions, int[] grantResults);
}
