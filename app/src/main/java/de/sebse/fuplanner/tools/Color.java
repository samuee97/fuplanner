package de.sebse.fuplanner.tools;

import android.graphics.Paint;

public interface Color {
    void setPaintColor(Paint paint);
}
