package de.sebse.fuplanner.tools.logging;

import android.util.Log;

public class Logger {
    private final String tag;

    public static Logger n(Object object) {
        return new Logger(object);
    }

    public Logger(Object object) {
        this.tag = getClassName(object);
    }

    private static String getClassName(Object object) {
        if (object instanceof String)
            return (String) object;
        else
            return object.getClass().getSimpleName();
    }

    public void d(Object... msg) {
        longLog(concat(msg), Log::d);
    }

    public void w(Object... msg) {
        longLog(concat(msg), Log::w);
    }

    public void e(Object... msg) {
        longLog(concat(msg), Log::e);
    }

    public void t(Object... msg) {
        longLog(concat(msg), Log::e);
        new Error().printStackTrace();
    }

    private void longLog(String content, LogInterface logInterface) {
        if (content.length() > 4000) {
            logInterface.run(tag, content.substring(0, 4000));
            longLog(content.substring(4000), logInterface);
        } else {
            logInterface.run(tag, content);
        }
    }

    private String concat(Object[] msg) {
        StringBuilder string = new StringBuilder();
        for (Object arg: msg)
            if (arg != null)
                string.append(arg.toString()).append(" ");
            else
                string.append("null ");
        return string.toString();
    }

    interface LogInterface {
        void run(String s1, String s2);
    }
}
