package de.sebse.fuplanner.services.kvv.types;

import com.google.android.gms.common.internal.Objects;

import java.io.Serializable;
import java.util.ArrayList;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.tools.ui.treeview.LayoutItemType;
import de.sebse.fuplanner.tools.ui.treeview.TreeNode;


public abstract class Resource implements Serializable {

    final String author;
    final long modifiedDate;
    final String title;
    final String url;
    private final boolean visible;
    private final String container;


    Resource(String author, String title, long modifiedDate, String url, boolean visible, String container) {
        this.author = author;
        this.title = title;
        this.modifiedDate = modifiedDate;
        this.url = url;
        this.visible = visible;
        this.container = container;
    }

    public String getAuthor() {
        return author;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getContainer() {
        return container;
    }

    public boolean isVisible() {
        return visible;
    }

    public abstract TreeNode getTreeNode();

    @Override
    public int hashCode() {
        return Objects.hashCode(getAuthor(), getContainer(), getModifiedDate(), getTitle(), getUrl());
    }

    public static class File extends Resource implements LayoutItemType {
        private final String type;

        public File(String author, String title, long modifiedDate, String url, boolean visible, String container, String type) {
            super(author, title, modifiedDate, url, visible, container);
            this.type = type;
        }

        @NonNull
        @Override
        public String toString() {
            return "Resource{" +
                    "author='" + author + '\'' +
                    ", modifiedDate=" + modifiedDate +
                    ", title='" + title + '\'' +
                    ", url='" + url + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }

        @Override
        public TreeNode getTreeNode() {
            return new TreeNode<>(this);
        }

        @Override
        public @LayoutRes int getLayoutId() {
            return R.layout.item_file;
        }
    }

    public static class Folder extends Resource implements LayoutItemType {

        private final ArrayList<Resource> children;
        public Folder(String author, String title, long modifiedDate, String url, boolean visible, String container) {
            super(author, title, modifiedDate, url, visible, container);
            children = new ArrayList<>();
        }

        public void add(Resource res){
            children.add(res);
        }

        public Resource get(int id){
            return children.get(id);
        }

        public int size(){
            return children.size();
        }

        @NonNull
        @Override
        public String toString() {
            return "Resource{" +
                    "author='" + author + '\'' +
                    ", modifiedDate=" + modifiedDate +
                    ", title='" + title + '\'' +
                    ", url='" + url + '\'' +
                    ", children='" + children + '\'' +
                    '}';
        }

        @Override
        public TreeNode getTreeNode() {
            TreeNode dir = new TreeNode<>(this);
            for (Resource res: children) {
                dir.addChild(res.getTreeNode());
            }
            return dir;
        }

        @Override
        public @LayoutRes int getLayoutId() {
            return R.layout.item_dir;
        }
    }
}


