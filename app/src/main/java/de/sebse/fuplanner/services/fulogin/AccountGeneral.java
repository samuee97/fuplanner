package de.sebse.fuplanner.services.fulogin;

public class AccountGeneral {
    public static final String ACCOUNT_TYPE = "de.sebse.fuplanner.fuauth";
    public static final String AUTHTOKEN_TYPE_KVV = "KVV";
    public static final String AUTHTOKEN_TYPE_BLACKBOARD = "Blackboard";
    public static final long SYNC_INTERVAL = 6 * 60 * 60; // defined in seconds
}
