package de.sebse.fuplanner.fragments.canteen;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import de.sebse.fuplanner.services.canteen.types.Canteen;
import de.sebse.fuplanner.tools.UtilsDate;

class DaySwitcherAdapter extends FragmentStatePagerAdapter {
    private Canteen mCanteen = null;

    DaySwitcherAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setModule(Canteen canteen) {
        mCanteen = canteen;
        this.setModule();
    }

    public void setModule() {
        this.notifyDataSetChanged();
    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return mCanteen == null ? 0 : mCanteen.size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        return MealFragment.newInstance(mCanteen.getId(), position);
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return UtilsDate.getModifiedDate(mCanteen.get(position).getCalendar().getTimeInMillis());
    }

}
