package de.sebse.fuplanner.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.tools.MainActivityListener;
import de.sebse.fuplanner.tools.logging.Logger;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnModulesFragmentInteractionListener}
 * interface.
 */
public class ModulesFragment extends Fragment {
    private OnModulesFragmentInteractionListener mListener;
    private final Logger log = new Logger(this);
    private ModulesAdapter adapter;
    private SwipeRefreshLayout swipeLayout;
    @Nullable private MainActivityListener mMainActivityListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ModulesFragment() {
    }

    public static ModulesFragment newInstance() {
        ModulesFragment fragment = new ModulesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new ModulesAdapter(mListener);
        recyclerView.setAdapter(adapter);

        // Getting SwipeContainerLayout
        swipeLayout = view.findViewById(R.id.swipe_container);
        // Adding Listener
        swipeLayout.setOnRefreshListener(() -> refresh(true));
        refresh(false);

        return view;
    }

    private void refresh(boolean forceRefresh) {
        if (mMainActivityListener != null) {
            mMainActivityListener.getKVV().modules().list().recv(success -> {
                adapter.setModules(success);
                //if (mMainActivityListener != null)
                //    mMainActivityListener.refreshNavigation();
                swipeLayout.setRefreshing(false);
            }, error -> {
                log.e(error.toString());
                swipeLayout.setRefreshing(false);
            }, forceRefresh);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnModulesFragmentInteractionListener) {
            mListener = (OnModulesFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnModulesFragmentInteractionListener");
        }
        if (context instanceof MainActivityListener) {
            mMainActivityListener = (MainActivityListener) context;
            mMainActivityListener.onTitleTextChange(R.string.courses);
        }
        else
            throw new RuntimeException(context.toString() + " must implement MainActivityListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mMainActivityListener = null;
    }

    public interface OnModulesFragmentInteractionListener {
        void onModulesFragmentInteraction(String id);
    }
}
